// Our Services
const buttons = document.querySelectorAll('.menu-button');
const contentItems = document.querySelectorAll('.content-item');

buttons.forEach((button) => {
    button.addEventListener('click', () => {
        buttons.forEach((btn) => btn.classList.remove('active'));
        button.classList.add('active');

        const contentId = button.getAttribute('data-content');
        contentItems.forEach((item) => item.classList.remove('active'));
        document.getElementById(contentId).classList.add('active');
    });
});

buttons[0].click();

// Our Amazing Work
const amazingGallery = document.querySelector('.amazing-gallery');
const loadMoreButton = document.querySelector('.load-more-button');
const filterButtons = document.querySelectorAll('.amazing-button');

let displayedImages = 0;
const imagesPerLoad = 12;

const images = [
    { category: 'graphic', src: './img/amazingwork/layer24.png' },
    { category: 'graphic', src: './img/amazingwork/layer25.png' },
    { category: 'web', src: './img/amazingwork/layer26.png' },
    { category: 'web', src: './img/amazingwork/layer27.png' },
    { category: 'web', src: './img/amazingwork/layer34.png' },
    { category: 'wordpress', src: './img/amazingwork/layer30.png' },
    { category: 'wordpress', src: './img/amazingwork/layer31.png' },
    { category: 'wordpress', src: './img/amazingwork/layer32.png' },
    { category: 'landing', src: './img/amazingwork/layer28.png' },
    { category: 'landing', src: './img/amazingwork/layer29.png' },
    { category: 'landing', src: './img/amazingwork/layer33.png' },
    { category: 'graphic', src: './img/amazingwork/graphic1.png' },
    { category: 'graphic', src: './img/amazingwork/graphi2.jpg' },
    { category: 'graphic', src: './img/amazingwork/graphic3.jpg' },
    { category: 'graphic', src: './img/amazingwork/graphic4.jpg' },

    
    { category: 'web', src: './img/amazingwork/landingpages1.png' },
    { category: 'web', src: './img/amazingwork/landingpages2.jpg' },
    { category: 'web', src: './img/amazingwork/landingpages3.png' },
    { category: 'web', src: './img/amazingwork/landingpages4.png' },
    
    
    { category: 'landing', src: './img/amazingwork/webdesign1.png' },
    { category: 'landing', src: './img/amazingwork/webdesign2.jpg' },
    { category: 'landing', src: './img/amazingwork/webdesign3.jpg' },
    { category: 'landing', src: './img/amazingwork/webdesign4.jpg' },
    
    
    { category: 'wordpress', src: './img/amazingwork/wordpress1.png' },
    { category: 'wordpress', src: './img/amazingwork/wordpress2.png' },
    { category: 'wordpress', src: './img/amazingwork/wordpress3.jpg' },
    { category: 'wordpress', src: './img/amazingwork/wordpress4.png' },
    
    
];

// Функция для загрузки и отображения изображений при нажатии на кнопку "Load More"
function loadMoreImages() {
    const imagesToLoad = images.slice(displayedImages, displayedImages + imagesPerLoad);
    imagesToLoad.forEach(image => {
        const imageContainer = document.createElement('div');
        imageContainer.classList.add('amazing-image', image.category);

        const imgElement = document.createElement('img');
        imgElement.src = image.src;

        const overlay = document.createElement('div');
        overlay.classList.add('overlay');

        const additionalImage = document.createElement('img'); // Создаем элемент для дополнительной картинки
        additionalImage.src = './img/amazingwork/icon.png'; // Устанавливаем путь к дополнительной картинке
        const text1 = document.createElement('h2');
        const text = document.createElement('p');
        text1.textContent = 'CREATIVE DESIGN';
        text.textContent = 'Web Design';

        overlay.appendChild(additionalImage); 
        overlay.appendChild(text1);
        overlay.appendChild(text);
        imageContainer.appendChild(imgElement);
        imageContainer.appendChild(overlay);

        amazingGallery.appendChild(imageContainer);
    });
    displayedImages += imagesToLoad.length;
    if (displayedImages >= images.length) {
        loadMoreButton.style.display = 'none';
    }
}

// Функция для фильтрации изображений по категории
function filterImages(category) {
    const allImages = document.querySelectorAll('.amazing-image'); // Выбираем все изображения на странице
    allImages.forEach(img => {
        if (category === 'all' || img.classList.contains(category)) {
            img.style.display = 'block'; // Отображаем изображение, если оно соответствует выбранной категории
        } else {
            img.style.display = 'none'; // Скрываем изображение, если оно не соответствует выбранной категории
        }
    });

    // Удаляем класс 'active' у всех кнопок фильтрации
    filterButtons.forEach(button => {
        button.classList.remove('active');
    });

    // Добавляем класс 'active' к выбранной кнопке фильтрации
    const selectedButton = document.querySelector(`.amazing-button[data-filter="${category}"]`);
    if (selectedButton) {
        selectedButton.classList.add('active');
    }
}

// Обработчик события для кнопки "Load More"
loadMoreButton.addEventListener('click', loadMoreImages);

// Обработчик события для кнопок фильтрации
filterButtons.forEach(button => {
    button.addEventListener('click', () => {
        const filterCategory = button.getAttribute('data-filter');
        filterImages(filterCategory); // Вызываем функцию фильтрации при клике на кнопку фильтрации
    });
});



// Начальная загрузка изображений
loadMoreImages(); // Вызываем функцию загрузки изображений при загрузке страницы

// Устанавливаем класс 'active' для кнопки "All" при загрузке страницы
document.querySelector('.amazing-button[data-filter="all"]').classList.add('active');


// каруслеь

const thumbnailImages = document.querySelectorAll('.thumbnails img');
thumbnailImages.forEach((image, index) => {
    image.addEventListener('click', () => {
        const slides = document.querySelectorAll('.slide');
        slides.forEach(slide => slide.removeAttribute('data-active'));
        slides[index].setAttribute('data-active', true);      
        const currentSlide = slides[index];
        titleElement.textContent = currentSlide.querySelector('h3').textContent;
        textElement.textContent = currentSlide.querySelector('p').textContent;
    });
});
function onNext() {
    const currentActiveIndex = [...document.querySelectorAll('.slide')].findIndex(slide => slide.hasAttribute('data-active'));
    const slides = document.querySelectorAll('.slide');
    slides[currentActiveIndex].removeAttribute('data-active');
    slides[(currentActiveIndex + 1) % slides.length].setAttribute('data-active', true);
}

function onPrev() {
    const currentActiveIndex = [...document.querySelectorAll('.slide')].findIndex(slide => slide.hasAttribute('data-active'));
    const slides = document.querySelectorAll('.slide');
    slides[currentActiveIndex].removeAttribute('data-active');
    slides[(currentActiveIndex - 1 + slides.length) % slides.length].setAttribute('data-active', true);
}